package service

import (
	"encoding/json"
	"io"
	"log"

	"github.com/360EntSecGroup-Skylar/excelize"

	"blog/service/model"
	"blog/service/cache_service"
)

type Tag struct {
	ID         int
	Name       string
	CreatedBy  string
	ModifiedBy string
	State      int

	PageNum  int
	PageSize int
}

func (t *Tag) ExistByName() (bool, error) {
	return model.ExistTagByName(t.Name)
}

func (t *Tag) ExistByID() (bool, error) {
	return model.ExistTagByID(t.ID)
}

func (t *Tag) Add() error {
	return model.AddTag(t.Name, t.State, t.CreatedBy)
}

func (t *Tag) Edit() error {
	data := make(map[string]interface{})
	data["modified_by"] = t.ModifiedBy
	data["name"] = t.Name
	if t.State >= 0 {
		data["state"] = t.State
	}

	return model.EditTag(t.ID, data)
}

func (t *Tag) Delete() error {
	return model.DeleteTag(t.ID)
}

func (t *Tag) Count() (int64, error) {
	return model.GetTagTotal(t.getMaps())
}

func (t *Tag) GetAll() ([]model.Tag, error) {
	var (
		tags, cacheTags []model.Tag
	)

	cache := cache_service.Tag{
		State: t.State,

		PageNum:  t.PageNum,
		PageSize: t.PageSize,
	}
	key := cache.GetTagsKey()
	if model.Exists(key) {
		data, err := model.Get(key)
		if err != nil {
			log.Println(err)
		} else {
			json.Unmarshal(data, &cacheTags)
			return cacheTags, nil
		}
	}

	tags, err := model.GetTags(t.PageNum, t.PageSize, t.getMaps())
	if err != nil {
		return nil, err
	}

	model.Set(key, tags, 3600)
	return tags, nil
}


func (t *Tag) Import(r io.Reader) error {
	xlsx, err := excelize.OpenReader(r)
	if err != nil {
		return err
	}

	rows := xlsx.GetRows("标签信息")
	for irow, row := range rows {
		if irow > 0 {
			var data []string
			for _, cell := range row {
				data = append(data, cell)
			}

			model.AddTag(data[1], 1, data[2])
		}
	}

	return nil
}

func (t *Tag) getMaps() map[string]interface{} {
	maps := make(map[string]interface{})
	maps["deleted_on"] = 0

	if t.Name != "" {
		maps["name"] = t.Name
	}
	if t.State >= 0 {
		maps["state"] = t.State
	}

	return maps
}
