package routers

import (
	"blog/util/setting"
	"blog/routers/api"
	"blog/routers/api/v1"
	"blog/middleware/jwt"
	"github.com/gin-gonic/gin"
	"blog/routers/consul/health"
	"github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	_ "blog/docs"
)

//返回gin实例
//Engine is the framework's instance, it contains the muxer, middleware and configuration settings. Create an instance of Engine, by using New() or Default()
func InitRouter() *gin.Engine{
	//returns a new blank Engine instance without any middleware attached
	r:=gin.New()
	////初始化logger中间件, log默认输出为os.Stdout
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	gin.SetMode(setting.RunMode)
	
	//swag可视化
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	//r.GET("/login",api.GetLogin)
	r.POST("/login",api.Login)
	//创建群组路由,用于统一规划, 比如统一通过token认证
	apiv1:=r.Group("/api/v1")
	apiv1.Use(jwt.JWT())
	{
		//获取标签列表
		apiv1.GET("/tags", v1.GetTags)
		//新建标签
		apiv1.POST("/tags", v1.AddTag)
		//更新指定标签
		apiv1.PUT("/tags/:id", v1.EditTag)
		//删除指定标签
		apiv1.DELETE("/tags/:id", v1.DeleteTag)
	}
	apihealth:=r.Group("/consul/health")
	{
	apihealth.GET("tags",health.Tags_Health)
	}
	return r
}