package model

import(
	"fmt"
	"log"
	"gorm.io/gorm"
	"gorm.io/driver/mysql"
	"github.com/gomodule/redigo/redis"
	"blog/util/setting"
	"net/url"
)

var DB *gorm.DB
var RedisClient redis.Pool

type Model struct {
	ID         int `gorm:"primary_key" json:"id"`
	CreatedOn  int `json:"created_on"`
	ModifiedOn int `json:"modified_on"`
	DeletedOn  int `json:"deleted_on"`
}

// type Model struct {
// 	ID int `gorm:"primary_key" json:"id"`
// 	CreatedOn int `json:"created_on"`
// 	ModifiedOn int `json:"modified_on"`
// }

func InitDB(){
	sec,err:=setting.Cfg.GetSection("database")
	host := sec.Key("HOST").String()
	port := sec.Key("PORT").String()
	database := sec.Key("TYPE").String()
	username := sec.Key("USER").String()
	password := sec.Key("PASSWORD").String()
	charset := sec.Key("CHARSET").String()
	loc := sec.Key("LOC").String()
	//dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=true&loc=%s",username,password,host,port,database,charset,url.QueryEscape(loc))
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=true&loc=%s",username,password,host,port,database,charset,url.QueryEscape(loc))
	log.Println("dsn:",dsn)
        db,err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
		if err != nil{
			log.Panic("fail to connect database,err:"+err.Error())
	}
	db.AutoMigrate(new(Tag),new(Auth))
	DB = db
}