package health

import(
	"net/http"
	"github.com/gin-gonic/gin"
	"blog/service"
	"blog/util/e"
)

func Tags_Health(c *gin.Context){
	appG := service.Gin{C: c}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]string{
		"ok": "true",
	})
}